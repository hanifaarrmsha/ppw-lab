Pendahuluan 
Authentication: proses verifikasi siapa anda dengan login 
Authorization: proses verifikasi bahwa kita punya akses untuk sesuatu. 
OAuth2: Oauth2 berupa sebuah authorization framework yang bertujuan agar suatu aplikasi mendapatkan akses tertentu dari user dalam suatu platform, dimana akses tersebut dapat digunakan dalam pemanggilan API yang disediakan oleh platform tersebut. 
Secara umum, Ouath2 memiliki beberapa roles, yaitu : 
Resource Owner : Resource owner merupakan pengguna platform yang memberikan akses terhadap suatu aplikasi. 
Resource/Authorization server :  Resource server merupakan penyedia platform, resource server berperan dalam menyediakan service Oauth2 dan API. 
Client/Application : Client merupakan aplikasi, baik dalam bentuk mobile maupun web application yang ingin menggunakan API dari resource/authorization server. 
 
 
 
Membuat halaman untuk Login menggunakan OAuth 
Mendaftarkan aplikasi ke facebook developer page 
Buka https://developers.facebook.com/ 
Daftar Sebagai Pengembang Facebook 
Buat Aplikasi baru dengan mengklik tombol Add a New App dapat ditemukan di kanan atas 
Masuk ke halaman aplikasi yang sudah dibuat dan pilih Add Product, kemudian tambahkan Facebook Login 
Pada bagian setting silakan tambahkan platform dan pilih website, lalu sesuaikan isi dari site url.  
Cara lain: Anda mungkin juga mendapati halaman Quickstart, pilih Web sebagai platform. 
Applikasi telah berhasil didaftarkan. 
Melakukan OAuth Login menggunakan Facebook 
Membuat button untuk login dalam HTML dengan event method, saya menulisnya method facebookLogin() 
Membuat method facebookLogin() yang memanggil method login yang sudah disediakan SDK facebook 
Function FB.login() ini juga memiliki parameter scope yang berguna untuk mengatur jenis permission apa saja yang aplikasi kita inginkan, sebagai contoh di atas kita telah menambah permission public_profile agar aplikasi kita dapat mengakses id, name, first_name, last_name dll milik kita (dapat diakses di https://developers.facebook.com/docs/facebook-login/permissions) 
Isi parameter dalam fungsi FB.login(callback) berupa fungsi callback, yang function callback itu merupakan function yang akan dilakukan apabila sudah berhasil login dan hanya akan dieksekusi apabila login berhasil. Contoh: 
 
function facebookLogin(){ 
     FB.login(function(response){ 
       console.log(response); 
     }, {scope:'public_profile,user_posts,publish_actions'}) 
   } 
 
Menampilkan informasi dari user yang login menggunakan API Facebook. 
Mengecek terlebih dahulu apakah user sudah connected atau belum menggunakan method SDK FB yaitu getLoginStatus, apabila connected maka: 
Pada method getUserData() menggunakan method api() pada SDK FB, terdapat beberapa param dalam method api() yaitu api(path, method, params, callback) 
Untuk pathnya, kita menggunakan '/me?fields=id,name,cover,about,email,picture.width(800).height(800),gender' dan method get karena ingin mengambil data, dan fungsi callback 
Melakukan post status facebook 
Membuat method postStatus yang mengambil message dari hal yang diketikkan oleh user dan menyimpannya di variable messages dan memanggil method postFeed 
Method postFeed akan menggunakan method SDK FB yaitu api() dengan path '/me/feed' , method post, params {message:message}, dan fungsi callback render(true) 
Menampilkan post status pada halaman lab_8.html 
Membuat method getUserFeed() yang didalamnya memanggil method SDK FB api() dengan path '/me/feed' dan method get, lalu fungsi callbacknya bernama fun 
Method getUserFeed dipanggil di method render dengan parameter berupa fungsi callback yang berfungsi untuk menampilkan tiap item feed. 
Melakukan Logout 
Membuat method facebookLogout() yang didalamnya mengecek terlebih dahulu apakah user telah connected atau belum, lalu memanggil FB.logout() yang didalamnya terdapat function callback. 
Implementasi css yang indah dan responsive 
Jawablah pertanyaan yang ada di dokumen ini dengan menuliskannya pada buku catatan atau pada source code kalian yang dapat ditunjukkan saat demo 
Dengan menambahkan permission user_posts dan publish_actions, apa saja yang dapat dilakukan oleh aplikasi kita menggunakan Graph API? 
User_posts: Menyediakan akses ke kiriman di Linimasa seseorang. Menyertakan kirimannya sendiri, kiriman yang menandainya, dan kiriman yang dibuat orang lain di Linimasanya. 
Publish_actions: Menyediakan akses untuk menerbitkan Kiriman, tindakan Graf Terbuka, dan aktivitas lain mewakili orang yang menggunakan aplikasi Anda. 
Melakukan delete status pada halaman facebook 
Implementasi tombol delete pada daftar post status 
Melakukan delete post status dengan menggunakan API Facebook yang ada. 
 
 
Rangkuman link: 
https://developers.facebook.com/docs/facebook-login/permissions - Referensi Izin - Facebook Login 
https://developers.facebook.com/docs/javascript/reference/v2.11 - Facebook SDK for JavaScript - Reference 
https://developers.facebook.com/docs/javascript/reference/FB.api/ - Graph API Request 
https://developers.facebook.com/docs/graph-api/using-graph-api/ - pilihan path untuk Graph API Request 