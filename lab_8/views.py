from django.shortcuts import render

# Create your views here.
def index(request):
    response = {'author' : 'Hanifa Arrumaisha'}
    response['login'] = True
    html = 'lab_8/lab_8.html'
    response['header_list'] = 'lab_8/partials/header.html'
    return render(request, html, response)
